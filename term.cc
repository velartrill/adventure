#include <termios.h>
#include <iostream>
#include <sys/ioctl.h>
#include <cstdio>

using std::string;

namespace term {
	unsigned int width() {
	   struct winsize ws;
	   ioctl(0, TIOCGWINSZ, &ws);
	   return ws.ws_col;
	}
	unsigned int height() {
	   struct winsize ws;
	   ioctl(0, TIOCGWINSZ, &ws);
	   return ws.ws_row;
	}
	void locate(unsigned int x, unsigned int y) {
		std::cout<<"\x1b["<<y<<";"<<x<<"H";
	}
	void header(string text) {
		int len = (width()-text.size())/2;
		std::cout<<"\x1b""7";
		locate(0,0);
		std::cout<<"\x1b[7m";
		for (int i=0;i<len;i++) putchar(' ');
		std::cout<<text;
		for (int i=0;i<len;i++) putchar(' ');
//		if (len*2<width()) putchar(' ');
		std::cout<<"\x1b[27m";
		std::cout<<"\x1b""8";
		
	}
	void clear() {
		std::cout<<"\x1b[2J";
		locate(0,0);
	}
}
