CC=clang
CFLAGS=""
LDFLAGS="-lstdc++"
PROD=$(basename $(pwd))
if [ `uname` == "Darwin" ]
then
	CFLAGS="$CFLAGS -DMACOSX"
fi

for f in *.cc
do
	if [ $f -nt $PROD ]; then
		printf "  \x1b[1;31mCC\x1b[0m  $f\n"
		$CC $CFLAGS -c $f
	fi
done;
printf " \e[1;32mLINK\e[0m $PROD\n"
$CC $LDFLAGS *.o -o $PROD
