#include <iostream>
#include <cstdio>
#include <string>

#include "interface.h"
#include "world.h"

int main(int argc, char** argv) {
	world w("world.dat");
	w.loc=&w.rooms[0];
	{ interface::terminal t(&w); w.intr=&t; }
	
	w.intr->mainloop();
	
	return 0;
}
