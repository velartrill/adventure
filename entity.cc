#include "entity.h"
#include <string>

std::string entity::getName(bool begin) {
	if (definite) {
		if (begin)
			return "The " + name;
		else
			return "the " + name;
	} else return name;
}

int container::weigh() {
	return 0;
}
