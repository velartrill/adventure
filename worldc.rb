#!/usr/bin/env ruby
#
# adventure World Compiler
#	
# This program takes as input a single XML file defining the world to build.
# The compiled adventure program is printed to stdout, while compilation
# status is printed to stderr.
# The format of the world XML file will be formally specified once the
# developer has any idea what that specification should be.
#

require "rexml/document"
include REXML

def strspec(str)
	s=[str.size].pack("L<")
	str.each_byte{|e| s+=[e].pack("C")}
	return s
end
#
#	WARNING: Make sure this is kept synchronized with the C++ header world_format.h,
# 	or all manner of depraved fuckery will occur
#
CMD_object		="\x01"
CMD_definite	="\x02"
CMD_proper		="\x03"
CMD_end			="\x00"

CMD_alias		="\x01"
CMD_takeable	="\x04"
CMD_light		="\x05"
CMD_lamp		="\x06"
CMD_weight		="\x07"

CMD_TYPE_entity ="\x00"
CMD_TYPE_container ="\x01"
CMD_TYPE_living ="\x02"
CMD_TYPE_person ="\x03"

CMD_FEAT_allowed	="\x00"
CMD_FEAT_scripted	="\x01"
CMD_FEAT_disallowed	="\x02"

class Room
	attr_accessor :name,:id,:desc,:detail,:definite,:proper,:objects
	def initialize(name,id,desc,detail,definite,proper)
		@name=name;
		@id=id;
		@desc=desc;
		@detail=detail;
		@definite=definite;
		@proper=proper;
		@objects=[]
	end
	def add(object)
		@objects.push object
	end
	def spec
		s=""
		s+=strspec(name)
		s+=strspec(desc)
		s+=strspec(detail)
		s+=CMD_definite	if definite
		s+=CMD_proper	if proper
		@objects.each{|o|
			s+=o.spec
		}
		s+=CMD_end
		return s
	end
end
class Entity
	attr_accessor :name,:id,:desc,:detail,:definite,:proper,:takeable,:intro,:aliases,:weight,:type
	def initialize(name,id,desc,detail,definite,proper,takeable,intro,aliases)
		@name=name;
		@id=id;
		@desc=desc;
		@detail=detail;
		@definite=definite;
		@proper=proper;
		@takeable=takeable;
		@intro=intro;
		@aliases=aliases
	end
	def spec
		s=CMD_object
		case type
			when "entity"
				s+=CMD_TYPE_entity			
			when "container"
				s+=CMD_TYPE_container
			when "living"
				s+=CMD_TYPE_living
			when "person"
				s+=CMD_TYPE_person
		end
		s+=strspec(name)
		s+=strspec(desc)
		s+=strspec(detail)
		s+=strspec(intro)
		s+=CMD_definite	if definite
		s+=CMD_proper	if proper
		if @takeable != CMD_FEAT_disallowed
			s+=CMD_takeable
			s+=@takeable
		end
		aliases.each {|a|
			s+=CMD_alias
			s+=strspec(a)
		}
		if @weight != nil
			s+=CMD_weight
			s+=[@weight].pack("L<")
		end
		s+=CMD_end
		return s
	end
end

def validate_descs(thing,e,hasIntro: false)
	if (!(thing.name && thing.id && thing.desc && thing.detail && (hasIntro ? thing.intro : true)))
		$stderr.puts "\x1b[31;1m{ERROR}\x1b[0;1m Invalid description specs:\x1b[21;33m"
		e.to_s.split("\n").each {|i| 
			$stderr.puts "\t"+i
		}
		$stderr.print "\x1b[0m"
		exit(1)
	end
end
$rooms=[]

xmlfile=File.new("world.xml");
xmldoc=Document.new(xmlfile);

root = xmldoc.root;

root.elements.each("room") {|e|
	room=Room.new(	e.attributes['name'],
					e.attributes['id'],
					e.attributes['desc'],
					e.attributes['detail'],
					e.attributes['definite']=="true",
					e.attributes['proper']=="true")
	
	validate_descs(room,e)
	$stderr.puts "\x1b[1mReading room \x1b[32m"+room.name+"\x1b[0m";
	e.elements.each("entity") { |o|
		al=[]
		o.elements.each("alias") {|a| al << a.text}
		object=Entity.new(
			o.attributes['name'],
			o.attributes['id'],
			o.attributes['desc'],
			o.attributes['detail'],
			o.attributes['definite']=="true",
			o.attributes['proper']=="true",
			(	o.attributes['take']=="allowed" ? CMD_FEAT_allowed :
				o.attributes['take']=="scripted" ? CMD_FEAT_scripted :
				CMD_FEAT_disallowed),
			o.attributes['intro'],
			al)
		if o.attributes['weight']
			object.weight=o.attributes['weight'].to_i
		end
		
		# FIXME add validation for type field
		if o.attributes['type']
			object.type=o.attributes['type']
		else
			object.type="entity"
		end
		
		$stderr.puts "  \x1b[31m*\x1b[0m  Adding entity \x1b[32;1m"+object.name+"\x1b[0m"
		validate_descs(object,o,hasIntro: true)
		room.add object
	}
	$rooms.push room
}

$exposition=[]
root.elements.each("exposition") { |e|
	$exposition << [e.attributes['id'], e.text]		#egads! beware the PHP demon of array-as-tuple-as-struct!
}
def getExpositionIndex(id)
	i=0
	$exposition.each { |e|
		if e[0]==id
			return i
		end
		i+=1
	}
	$stderr.puts "Cannot find EXPO ID "+id
	exit(2)
end
def getRoomIndex(id)
	i=0
	$rooms.each { |r|
		if r.id==id
			return i
		end
		i+=1
	}
end
def getEntityIndex(id)
	pts=id.split('.')
	room=getRoomIndex(pts[0])
	i=0
	$rooms[room].objects.each {|e|
		if e.id==pts[1]
			return i
		end
		i+=1
	}
	$stderr.puts "Cannot find OBJECT ID "+id
	exit(2)
end


#header
file=[
	#magic bytes
		0xFE,0x44,
	#parameters
		getEntityIndex(root.attributes['player']),
		root.attributes['intro'] ? getExpositionIndex(root.attributes['intro'])+1 : 0,
].pack("CCL<L<")

file+=[$rooms.size].pack("L<")
$rooms.each { |room|
	$stderr.puts "\x1b[1mWriting room \x1b[32m"+room.name+"\x1b[0m"
	file+=room.spec
}


file+=[$exposition.size].pack("L<")
$exposition.each { |ex|
	file+=strspec(ex[1])
}

print file
