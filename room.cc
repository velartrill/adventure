#include "room.h"
#include <string>
using std::string;

string room::getName(bool begin) {
	if (definite) {
		if (begin)
			return "The " + name;
		else
			return "the " + name;
	} else return name;
}