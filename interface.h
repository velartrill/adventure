#pragma once
#include "world.h"
#include <string>
#include <vector>

class world;
namespace interface {
	class interface { public:
		world* w;
		interface(world* _w);
		virtual void	publish(std::string)= 0,
						sep()				= 0,
						changeRoom(room*)	= 0,
						mainloop() 			= 0;
						
		std::vector<std::string> parse(std::string str);
	};
	class terminal : public interface { public:
		terminal(world* _w);
		void publish(std::string str);
		void changeRoom(room* r);
		void mainloop();
		void sep();
	};
};