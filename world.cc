#include "world.h"
#include "world_format.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cassert>
using std::string;

inline unsigned int read_byte(unsigned char*& p) {
	return *p++;
}
inline entity::feature read_feature(unsigned char*& p) {
	switch (read_byte(p)) {
		case 0: return entity::allowed;
		case 1: return entity::scripted;
		case 2: return entity::disallowed;		// may or may not use this; FIXME: delete if unused in final version
	}
	std::cerr<<"Invalid feature spec in world data file."<<std::endl;
	exit(1);
}
inline unsigned int read_word(unsigned char*& p) {	// read a little-endian int and convert it to native endian
	return read_byte(p) | (read_byte(p) << 8);
}
inline unsigned int read_dword(unsigned char*& p) {	// read a little-endian int and convert it to native endian
	return read_word(p) | (read_word(p) << 16);
}

string read_string(unsigned char*& p) {
	string rv="";
	for (unsigned int j=read_dword(p); j!=0; j--) {		// Read room name
		rv+=*p++;
	}
	return rv;
}

world::world(string name) {
	size_t size;
	unsigned char* file = NULL;
	{	std::ifstream f(name.c_str(),std::ios::in | std::ios::binary | std::ios::ate);
		if(f.is_open()) {
			size=f.tellg();
			file=new unsigned char[size];
			f.seekg(0,std::ios::beg);
			f.read((char*)file,size);
			f.close();
		} else {
			std::cerr<<"Cannot read world file.\n";
			exit(1);
		}
	}
	unsigned char* p = file;
	if (!(read_byte(p) == 0xFE && read_byte(p) == 0x44)) {
		std::cerr << "World file format not recognized.\n";
		exit(1);
	}
	intro_id = read_dword(p);
	unsigned int player_id = read_dword(p);
	std::vector<entity*> e_store;
	for (unsigned int i=read_dword(p); i!=0; i--) {			// Determine the number of rooms
		room r;
		r.name=read_string(p);
		r.desc=read_string(p);
		r.detail=read_string(p);
		r.definite=false;
		r.proper=false;
		r.light=false;
		{bool reading_room_cmds=true;
		while(reading_room_cmds) {
			switch(read_byte(p)) {
				case room_cmd::object: {
					entity* en;
					switch (unsigned char c = read_byte(p)) {	//determine entity type
						case entity_cmd::type::entity:
							en = new entity;
							en->type=entity::plain;
						break;
						
						case entity_cmd::type::container:
							en = new container;
							en->type=entity::container;
						break;
						
						case entity_cmd::type::living:
							en = new living;
							en->type=entity::living;
						break;
						
						case entity_cmd::type::person:
							en = new person;
							en->type=entity::person;
						break;
						
						default:
							std::cerr<<"ERROR: Invalid object type "<<(int)c<<"!\n";
							exit(3);
						break;
					}
					entity& e = *en;
					// FIXME: this needs to create the correct class of object.
					// That's gonna hurt.
					e.name=read_string(p);
					e.desc=read_string(p);
					e.detail=read_string(p);
					e.intro=read_string(p);
					e.definite=false;
					e.proper=false;
					e.takeable=entity::disallowed;
					e.weight=1;
					{bool reading_entity_cmds=true;
					while(reading_entity_cmds) switch (read_byte(p)) {
						case room_cmd::definite:	e.definite=true; break;
						case room_cmd::proper:		e.proper=true; break;
						case entity_cmd::alias:		e.aliases.push_back(read_string(p)); break;
						case entity_cmd::takeable:	e.takeable=read_feature(p); break;
						case entity_cmd::weight:		e.weight=read_dword(p);break;
						case room_cmd::end: 		reading_entity_cmds=false; break;
					}}
					e_store.push_back(&e);
					r.entities.push_back(&e);
				break; }
				case room_cmd::definite:	r.definite=true; break;
				case room_cmd::proper:		r.proper=true; break;
				case room_cmd::light:		r.light=true; break;
				case room_cmd::end:			reading_room_cmds=false; break;
			}
		}}
		player=(person*)e_store[player_id];	// yes, this is evil. yes, it depends on correct formatting of the world file.
											// no, I'm not sorry.
		std::cerr<<player->name<<'\n';
		rooms.push_back(r);
	}
		
	for (unsigned int i=read_dword(p); i!=0; i--) {		// get number of exposition entries
		exposition.push_back(read_string(p));
	}
	delete[] file;
}

void world::go(room::direction dir) {
	if (loc->exits.find(dir) != loc->exits.end()) {
		if (loc->exits[dir].locked==false) {
			loc=loc->exits[dir].dest;
			enterRoom(loc);
		} else {
			intr->publish("That exit is locked.");
		}
	} else {
		intr->publish("You can't go that way.");
	}
}
void world::entities() {
	if (loc->entities.size()>0) {
		for (std::vector<entity*>::iterator i=loc->entities.begin();i!=loc->entities.end();i++) {
			if (*i == player) continue; // don't describe the player
			intr->sep();
			intr->publish((*i)->intro);
		}
	}
}
void world::init() {
	if (intro_id>0) {
		intr->publish(exposition[intro_id-1]);
	}
	enterRoom(loc);
}
void world::describe() {
	intr->publish(loc->detail);
	entities();
	if (loc->exits.size()>0) {
		intr->sep();
		for(int i=room::north; i<=room::down; i++) {
			if (loc->exits.find((room::direction)i) != loc->exits.end()) {
				if (loc->exits[(room::direction)i].desc!="")
					intr->publish(loc->exits[(room::direction)i].desc);
				else {
					string pref;
					switch (i) {
						case room::north:	pref="To the north is "; break;
						case room::east:	pref="To the east is "; break;
						case room::west:	pref="To the west is "; break;
						case room::south:	pref="To the south is "; break;
						
						case room::northwest:	pref="To the northwest is "; break;
						case room::northeast:	pref="To the northeast is "; break;
						case room::southwest:	pref="To the southwest is "; break;
						case room::southeast:	pref="To the southeast is "; break;
						
						case room::out: pref="Outside is "; break;
						case room::up: pref="Above is "; break;
						case room::down: pref="Below is "; break;
					}
					intr->publish(pref + loc->exits[(room::direction)i].dest->getName(false) + ".");
				}
			}
		}
	}
}
void world::enterRoom(room* r){
	intr->changeRoom(r);
	intr->publish(r->visited?r->desc:r->detail);
	entities();
	r->visited=true;
}
string to_lower(const string& str) {
	string rv = str;
	for (size_t i=0;i<rv.size();i++) {
		if (rv[i] >= 'A' && rv[i] <= 'Z')
			rv[i]+=0x20;
	}
	return rv;
}
entity* world::getEntityByName(const string& obj, std::vector<entity*>::iterator* index) {
	for (std::vector<entity*>::iterator i=loc->entities.begin();i!=loc->entities.end();i++) {
		if (to_lower(obj)==to_lower((*i)->name)) { 
			if (index!=NULL) *index=i;
			return *i;
		} else {
			for (std::vector<string>::iterator j=(*i)->aliases.begin(); j!=(*i)->aliases.end();j++) {
				if(to_lower(obj)==to_lower(*j)) {
					if (index!=NULL) *index=i;
					return *i;
				}
			}
		}
	}
	return NULL;
}
entity* world::getEntityByVector(const std::vector<string>& vec, unsigned char start, unsigned char* pos, std::vector<entity*>::iterator* index, std::string* name) {
	if (vec.size()>2) {
		string obj;
		for (int i=start;i<vec.size();i++) {
			obj+=vec[i];
			if(entity* e = getEntityByName(obj,index)) {
				if (name!=NULL) *name=obj;
				*pos=i+1; return e;
			}
			obj+=" ";
		}
	} else {
		*pos=start+1;
		if (name!=NULL) *name=vec[start];
		return getEntityByName(vec[start],index);
	}
	return NULL; // size > 2 && nothing found
}
void world::eval(std::vector<string>& cmd) {
	if(cmd.size()==1) {
		if (cmd[0]=="n" || cmd[0]=="north")	{ go(room::north); return; }	else
		if (cmd[0]=="w" || cmd[0]=="west")	{ go(room::west); return; }		else
		if (cmd[0]=="e" || cmd[0]=="east")	{ go(room::east); return; }		else
		if (cmd[0]=="s" || cmd[0]=="south")	{ go(room::south); return; }	else
		
		if (cmd[0]=="ne" || cmd[0]=="northeast")	{ go(room::northeast); return; }	else
		if (cmd[0]=="nw" || cmd[0]=="northwest")	{ go(room::northwest); return; }	else
		if (cmd[0]=="se" || cmd[0]=="southeast")	{ go(room::southeast); return; }	else
		if (cmd[0]=="sw" || cmd[0]=="southwest")	{ go(room::southwest); return; }	else
		
		if (cmd[0]=="o" || cmd[0]=="out")	{ go(room::out); return; }	else
		if (cmd[0]=="u" || cmd[0]=="up")	{ go(room::up); return; }	else
		if (cmd[0]=="d" || cmd[0]=="down")	{ go(room::down); return; }	else
		
		if (cmd[0]=="l" || cmd[0] == "look") {
			describe(); return;
		} else
		if (cmd[0]=="take" || cmd[0]=="get") {
			intr->publish("Take what?");
			return;
		} else if (cmd[0]=="i" || cmd[0]=="inventory") {
			if (player->entities.size() > 0) {
				intr->publish("Currently carrying: ");
				for (std::vector<entity*>::iterator i = player->entities.begin(); i!=player->entities.end(); i++) {
					intr->publish("\t * "+(*i)->name);
				}
			} else {
				intr->publish("You aren't carrying anything.");
			}
			return;
		}
	} else {
		if (cmd[0]=="look" || cmd[0]=="examine") {
			unsigned char pos;
			entity* e = getEntityByVector(cmd,cmd[1]!="at"?1:2,&pos,NULL);
			if (pos<cmd.size()) {
				intr->publish("I'm not sure what you meant by \""+cmd[pos]+".\"");
				return;
			}
			if (e!=NULL) {
				intr->publish(cmd[0]!="examine"?e->desc:e->detail);
			} else {
				intr->publish("I don't see anything like that here.");
			}
			return;
		} else if (cmd[0]=="take" || cmd[0]=="get") {
			unsigned char pos;
			std::vector<entity*>::iterator index;
			std::string name;
			entity* e = getEntityByVector(cmd,cmd[1]!="the"?1:2,&pos,&index,&name);
			if (e!=NULL) {
				if (e->takeable==entity::allowed) {
					if (player->weigh() + e->weight <= player->maxweight) {
						loc->entities.erase(index);
						player->entities.push_back(e);
						intr->publish("You take the "+name+".");
					} else {
						intr->publish("That's too heavy for you to carry!");
					}
				} else if (e->takeable==entity::scripted) {
					// invoke bytecode interpreter
				} else {
					intr->publish("You can't take the "+name+".");
				}
			} else {
				intr->publish("I don't see that anywhere.");
			}
			return;
		}
	}
	intr->publish("I have no earthly clue what you just said.");
}
