#pragma once
#include <string>
#include <map>
#include <vector>
#include "entity.h"

class exit { public:
	bool			locked;
	entity*			key;
	struct room* 	dest;
	std::string		desc;
};

class room { public:
	typedef enum direction {
		north, west, east, south,
		northeast, northwest, southeast, southwest,
		out, up, down
	} direction;
	std::string name;
	std::string desc;
	std::string detail;
	std::map<direction, struct exit> exits;
	std::vector<entity*> entities;
	bool visited;
	bool definite; // definite article used in name
	bool proper;	// lock capitalization
	bool light;
	std::string getName(bool begin);
};
