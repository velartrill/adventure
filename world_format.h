namespace room_cmd {
	enum {
		end			= 0x00,
		object		= 0x01,	// Add an object
		definite	= 0x02,	// Use the definite article when referring to this room
		proper		= 0x03,	// Use the capitalization specified in the file
		light		= 0x05, // room is lit
	};
}
namespace entity_cmd {
	enum {
		alias		= 0x01,	// An extra name by which a thing can be referred to
			// definite
			// proper
		takeable	= 0x04,	// can be picked up by the player
		light		= 0x05, // can be seen in dark
		lamp		= 0x06, // provides light
		weight		= 0x07, // sets weight (dword)
	};
	namespace type {
		enum {
			entity		= 0x00,
			container	= 0x01,
			living		= 0x02,
			person		= 0x03,
		};
	}
}
