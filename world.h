#pragma once
#include "room.h"
#include "interface.h"
#include "entity.h"
#include "person.h"
#include <string>
#include <vector>

namespace interface { class interface; }
class world { public:
	interface::interface* intr;
	std::vector<room> rooms;
	std::vector<std::string> exposition;
	unsigned int intro_id;	// 0 = no exp; 1 is subtracted to retrieve index
	struct room* loc;
	person* player;		// it's a pointer only to enable switching perspectives. Not sure how good an idea this is.
	void enterRoom(struct room* r);
	void eval(std::vector<std::string>& cmd);
	void go(room::direction dir);
	void describe();
	void entities();
	void init();
	entity* getEntityByName(const std::string& name, std::vector<entity*>::iterator* index = NULL);
	entity* getEntityByVector(
		const std::vector<std::string>& vec,
		unsigned char start,
		unsigned char* pos,
		std::vector<entity*>::iterator* index = NULL,
		std::string* name = NULL);
	world(std::string);
};
