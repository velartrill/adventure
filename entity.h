#pragma once
#include <string>
#include <vector>

class entity { public:
	enum { plain, container, living, person } type;
	std::string name,intro,desc,detail;
	std::vector<std::string> aliases;
	typedef enum { allowed = 0x00, scripted = 0x01, disallowed = 0x02 } feature;
	bool definite,proper;
	feature takeable;
	int weight;
	std::string getName(bool begin);
};

class container : public entity { public:
	std::vector<entity*> entities;
	int maxweight;
	int weigh();
};
