#include "interface.h"
#include "term.h"
#include <iostream>
#include <string>
#include <cstdio>

using std::string;

namespace interface {
	interface::interface(world* _w) {
		w=_w;
	}
	std::vector<string> interface::parse(string str) {
		std::vector<string> tokens;
		{	string ctoken="";
			for (int i=0;i<str.size();i++) {
				if (str[i]!=' '&&str[i]!='\t') {
					ctoken+=str[i];
				} else if(ctoken=="") continue;
				else {
					tokens.push_back(ctoken);
					ctoken="";
				}
			}
			if(ctoken!="") tokens.push_back(ctoken);
		}
		return tokens;
	}
	terminal::terminal(world* _w) : interface(_w) {
		term::clear();
		term::locate(1,2);
	}
	void terminal::publish(string str) {
		std::cout << str << std::endl;
	}
	void terminal::changeRoom(room* r) {
		term::header(r->name);
	}
	void terminal::mainloop() {
		w->init();
		for(;;) {
			string str;
		#ifndef MACOSX	// Terminal.app can't handle ANSI dim
			std::cout<<"\x1b[2m> "; // so we do the opposite
		#else
			std::cout<<"\x1b[1m> ";
		#endif
			std::getline(std::cin,str);
			std::cout<<"\x1b[0m";
			if (str.size()>0) {
				std::vector<string> s = parse(str);
				w->eval(s);
			}
		}
	}
	void terminal::sep() {
		putchar('\n');
	}
}
