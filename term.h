#include <string>

namespace term {
	unsigned int width();
	unsigned int height() ;
	void locate(unsigned int x, unsigned int y);
	void header(std::string text);
	void clear();
}